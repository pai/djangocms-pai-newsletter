from django.apps import AppConfig


class DjangocmsPaiNewsletterConfig(AppConfig):
    name = 'djangocms_pai_newsletter'
